﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Toilo {
    public class Menu: Level {
        public Menu(ToiloGame game): base(game, null, null) {
        }

        public override void Update(GameTime gameTime) {
            if (Keyboard.GetState().IsKeyDown(Keys.Enter)) {
                game.ChangeLevel(nextLevel);
            }
            if (texts[0].Destroy) {
                texts[1].Update(gameTime);
            }
            texts[0].Update(gameTime);
        }
        public override void Draw(SpriteBatch spriteBatch) {
            foreach (GameText text in texts) {
                text.Draw(spriteBatch);
            }
        }

        public override void Initialize() {
            texts = new List<GameText>();
            texts.Add(new GameText(new Vector2(ToiloGame.Width / 2, ToiloGame.Height / 2), ToiloGame.DefaultFont, "Shitty Space", 0.1f));
            texts.Add(new GameText(new Vector2(ToiloGame.Width / 2, 100+ (ToiloGame.Height / 2)), ToiloGame.DefaultFont, "Press Enter to Start", 0.05f));
        }
    }
}
