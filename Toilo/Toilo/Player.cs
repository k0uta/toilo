﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Toilo {
    public class Player: BaseObject {
        private const int pWidth = 32;
        private const int pHeight = 32;
        private const int frameHeight = 32;
        private const int frameWidth = 32;
        private const float fpsTime = 0.1f;
        private const int playerLives = 3;
        private const float playerSpeed = 200f;
        private const float invulTime = 3f;
        private const float shotInterval = 0.1f;
        
        protected int lives;

        private float timer = 0f;
        private float shotTimer = 0f;
        private float invulTimer = 0f;
        private KeyboardState lastKstate;
        private Texture2D shotTexture;
        private float alpha = 0;
        private float aIncrement = 40f;
        private List<PlayerShot> shots;
        protected int score = 0;

        public Player(Texture2D tex, Vector2 pos, Texture2D shotTexture): base(tex, pos, pWidth, pHeight) {
            lives = playerLives;
            sourceBox = new Rectangle(0, 0, frameWidth, frameHeight);
            this.shotTexture = shotTexture;
            shots = new List<PlayerShot>();
        }

        public override void Update(GameTime gameTime) {
            if (invulTimer > 0) {
                invulTimer -= (float)gameTime.ElapsedGameTime.TotalMilliseconds / 1000f;
            }
            
            if (timer > fpsTime) {
                timer = 0f;
                if (sourceBox.Y / frameHeight >= texture.Height/ frameHeight- 1) {
                    sourceBox = new Rectangle(sourceBox.X, 0, frameWidth, frameHeight);
                }
                else {
                    sourceBox = new Rectangle(sourceBox.X, sourceBox.Y + frameHeight, frameWidth, frameHeight);
                }
            }
            else {
                timer += (float)gameTime.ElapsedGameTime.Milliseconds / 1000;
            }
            
            KeyboardState currentKstate = Keyboard.GetState();
            Vector2 newPosition = position;
            float deltaTime = (float)gameTime.ElapsedGameTime.TotalMilliseconds/1000;
            float currentSpeed = playerSpeed;

            if (currentKstate.IsKeyDown(Keys.LeftShift)) {
                currentSpeed *= 2f;
            }
            if (currentKstate.IsKeyDown(Keys.Up) || currentKstate.IsKeyDown(Keys.W)) {
                newPosition.Y -= currentSpeed * deltaTime;
            }
            if (currentKstate.IsKeyDown(Keys.Down) || currentKstate.IsKeyDown(Keys.S)) {
                newPosition.Y += currentSpeed * deltaTime;
            }
            if (currentKstate.IsKeyDown(Keys.Left) || currentKstate.IsKeyDown(Keys.A)) {
                newPosition.X -= currentSpeed * deltaTime;
            }
            if (currentKstate.IsKeyDown(Keys.Right) || currentKstate.IsKeyDown(Keys.D)) {
                newPosition.X += currentSpeed * deltaTime;
            }

            if (newPosition.X < ToiloGame.CameraDistance.X) {
                newPosition.X = ToiloGame.CameraDistance.X;
            }
            if (newPosition.X > ToiloGame.Width + ToiloGame.CameraDistance.X - frameWidth) {
                newPosition.X = ToiloGame.Width + ToiloGame.CameraDistance.X - frameWidth;
            }
            if (newPosition.Y < 0) {
                newPosition.Y = 0;
            }
            if (newPosition.Y > ToiloGame.Height-frameHeight) {
                newPosition.Y = ToiloGame.Height-frameHeight;
            }

            newPosition.X += (float)gameTime.ElapsedGameTime.TotalMilliseconds / 3;
            position = newPosition;
            shotTimer += deltaTime;

            foreach (PlayerShot shot in shots) {
                shot.Update(gameTime);
                if (shot.Position.X > ToiloGame.Width+ToiloGame.CameraDistance.X + 50) {
                    shot.DestroyShot();
                }
            }
            shots.RemoveAll(item => item.Destroy);

            if (currentKstate.IsKeyDown(Keys.Space)) {
                if (shotTimer > shotInterval) {
                    shotTimer = 0f;
                    PlayerShot pShot = new PlayerShot(shotTexture, position + new Vector2(pWidth, ((pHeight-shotTexture.Height)/2) + shotTexture.Height));
                    shots.Add(pShot);
                    ToiloGame.PlaySound("PlayerShot", 0.05f);
                }
            }


            lastKstate = currentKstate;
            base.Update(gameTime);
        }

        public override void Draw(SpriteBatch spriteBatch) {
            if (Invulnerable) {
                alpha += aIncrement;
                if (alpha > 255 || alpha<0)
                    aIncrement*=-1;
                spriteBatch.Draw(texture, position, sourceBox, new Color(255,255,255,(byte) MathHelper.Clamp(alpha,0, 255)));
            }
            else {
                spriteBatch.Draw(texture, position, sourceBox, Color.White);
            }
            foreach (PlayerShot shot in shots) {
                shot.Draw(spriteBatch);
            }
            base.Draw(spriteBatch);
        }

        public void DestroyPlayer() {
            ToiloGame.PlaySound("PlayerExplosion", 0.5f);
            SetInvul();
            lives--;
            ResetPosition();
        }

        public bool Invulnerable {
            get { return invulTimer > 0; }
        }
        public List<PlayerShot> Shots {
            get { return shots; }
        }
        public void ResetPosition() {
            position = new Vector2(ToiloGame.CameraDistance.X, ToiloGame.Height / 2);
        }
        public int Score {
            get { return score; }
            set { score = value; }
        }
        public int Lives {
            get { return lives; }
        }

        public void SetInvul(float time = invulTime) {
            invulTimer = time;
        }

        public void Reset() {
            lives = playerLives;
            score = 0;
        }
    }
}
