﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Toilo {
    public class AnimatedObject: BaseObject {
        private const int frameW = 48;
        private const int frameH = 48;
        private float fpsTime;

        private float fpsTimer = 0f;

        private bool destroy = false;

        public AnimatedObject(Texture2D tex, Vector2 pos, float fpsTime=0.1f): base(tex, pos, frameW, frameH) {
            sourceBox = new Rectangle(0, 0, frameH, frameW);
            this.fpsTime = fpsTime;
        }

        public override void Update(GameTime gameTime) {
            if (fpsTimer > fpsTime) {
                fpsTimer = 0f;
                if (sourceBox.Y / frameH >= texture.Height / frameH - 1) {
                    sourceBox = new Rectangle(0, 0, frameW, frameH);
                    destroy = true;
                }
                else {
                    sourceBox = new Rectangle(sourceBox.X, sourceBox.Y+frameH, frameW, frameH);
                }
            }
            else {
                fpsTimer += (float)gameTime.ElapsedGameTime.Milliseconds / 1000;
            }
            base.Update(gameTime);
        }

        public override void Draw(SpriteBatch spriteBatch) {
            spriteBatch.Draw(texture, position, sourceBox, Color.White);
            base.Draw(spriteBatch);
        }

        public bool Destroy {
            get { return destroy; }
            set { destroy = value; }
        }
    }
}
