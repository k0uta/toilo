﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Toilo {
    public class Enemy: BaseObject {
        protected bool destroy=false;
        protected float life;
        private const float fpsTime = 0.1f;

        private float fpsTimer = 0f;

        public Enemy(Texture2D texture, Vector2 position, int width, int height, float life=50f): base(texture, position, width, height) {
            this.life = life;
        }

        public override void Update(GameTime gameTime) {
            if (fpsTimer > fpsTime) {
                fpsTimer = 0f;
                if (sourceBox.X / width >= texture.Width / width- 1) {
                    sourceBox = new Rectangle(0, 0, width, height);
                }
                else {
                    sourceBox = new Rectangle(sourceBox.X + width, sourceBox.Y, width, height);
                }
            }
            else {
                fpsTimer += (float)gameTime.ElapsedGameTime.Milliseconds / 1000;
            }
            base.Update(gameTime);
        }

        public override void Draw(SpriteBatch spriteBatch) {
            spriteBatch.Draw(texture, position, sourceBox, Color.White);
            base.Draw(spriteBatch);
        }

        public bool Destroy {
            get { return destroy; }
        }

        public float Life {
            get { return life; }
            set { life = value; }
        }

        public void DestroyEnemy() {
            destroy = true;
        }
    }
}
