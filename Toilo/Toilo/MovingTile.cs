﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Toilo {
    public class MovingTile: Tile {
        protected Vector2 direction;
        protected Vector2 maxDistance;
        private Vector2 currentDistance = Vector2.Zero;

        private const float speed = 100f;


        public MovingTile(Texture2D texture, Vector2 position, Vector2 direct, int qt): base(texture, position) {
            this.direction = direct;
            this.maxDistance = new Vector2(width *qt, height * qt);
        }

        public override void Update(GameTime gameTime) {
            float deltaTime = (float)gameTime.ElapsedGameTime.TotalMilliseconds / 1000;
            if (Math.Abs(currentDistance.Y) > maxDistance.Y || Math.Abs(currentDistance.X) > maxDistance.X) {
                direction *= -1f;
            }

            position += direction * deltaTime * speed;
            currentDistance += direction * deltaTime * speed;
            base.Update(gameTime);
        }
    }
}
