﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Toilo {
    public class GameText: BaseObject {
        private String currentText;
        protected float typeInterval;
        protected float destroyTime;
        private Vector2 absolutePos;
        protected bool destroy = false;

        protected String completeText;
        private float timer=0f;
        private SpriteFont spriteFont;

        public GameText(Vector2 aPosition, SpriteFont sFont, String text, float tInterval=0.2f, float destroyTime=1f): base(null, aPosition+ToiloGame.CameraDistance, 0, 0) {
            this.typeInterval = tInterval;
            this.completeText = text;
            this.spriteFont = sFont;
            this.absolutePos = aPosition;
            this.currentText = "";
            this.destroyTime = destroyTime;
        }

        public override void Update(GameTime gameTime) {
            if (currentText.Length != completeText.Length) {
                if (timer >= typeInterval) {
                    timer = 0f;
                    if (completeText[currentText.Length] != ' ') {
                        ToiloGame.PlaySound("TextWrite", 0.5f);
                    }
                    else {
                        timer = typeInterval;
                    }
                    currentText += completeText[currentText.Length];
                }
                else {
                    timer += (float)gameTime.ElapsedGameTime.TotalMilliseconds / 1000;
                }
            }
            else {
                if (timer >= destroyTime) {
                    destroy = true;
                }
                else {
                    timer += (float)gameTime.ElapsedGameTime.TotalMilliseconds / 1000;
                }
            }
            position = absolutePos + ToiloGame.CameraDistance - (Vector2.UnitX*spriteFont.MeasureString(completeText).X)/2;
            base.Update(gameTime);
        }

        public override void Draw(SpriteBatch spriteBatch) {
            spriteBatch.DrawString(spriteFont, currentText, position, Color.White);
            base.Draw(spriteBatch);
        }

        public bool Destroy {
            get { return destroy; }
        }
    }
}
