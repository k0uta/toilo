﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Toilo {
    public class Level {
        private const int hudHeight = 5;

        protected ToiloGame game;
        protected Texture2D levelMap;
        protected Level nextLevel;
        protected Player player;
        protected List<Tile> tiles;
        protected List<Coin> coins;
        protected List<GameText> texts;
        protected List<Enemy> enemies;
        protected List<AnimatedObject> animations;
        protected HUD hud;
        protected String levelName;
        protected bool levelComplete = false;
        protected bool gameOver = false;

        public Level(ToiloGame game, Texture2D mapT, String levelName) {
            this.game = game;
            this.levelMap = mapT;
            this.levelName = levelName;
        }

        public virtual void Initialize() {
            texts = new List<GameText>();
            texts.Add(new GameText(new Vector2(ToiloGame.Width/2, ToiloGame.Height/2),ToiloGame.DefaultFont,levelName,0.05f));
            texts.Add(new GameText(new Vector2(ToiloGame.Width / 2, ToiloGame.Height / 2), ToiloGame.DefaultFont, "Start", 0.05f));
            LoadMap(levelMap);
            gameOver = false;
            player.SetInvul();
            player.ResetPosition();
            animations = new List<AnimatedObject>();
            levelComplete = false;
            hud = new HUD(ToiloGame.GetTexture("HUD"),ToiloGame.GetTexture("Playerv3"),levelName);
        }

        public virtual void GameOver() {
            player.Shots.RemoveAll(shot => shot != null);
            texts.Add(new GameText(new Vector2(ToiloGame.Width / 2, ToiloGame.Height / 2), ToiloGame.DefaultFont, "Game Over", 0.2f));
            gameOver = true;
        }

        public virtual void Update(GameTime gameTime) {
            if (player.Lives >= 0) {
                player.Update(gameTime);
            }
            tiles.RemoveAll(item => item.Destroy);
            coins.RemoveAll(item => item.Destroy);
            texts.RemoveAll(item => item.Destroy);
            enemies.RemoveAll(item => item.Destroy);
            animations.RemoveAll(item => item.Destroy);

            foreach (Enemy enemy in enemies) {
                if (ToiloGame.InBounds(enemy.Position)) {
                    if (player.Collide(enemy)) {
                        if (!player.Invulnerable) {
                            animations.Add(new AnimatedObject(ToiloGame.GetTexture("Explosion"), player.Position));
                            player.DestroyPlayer();
                            if (player.Lives < 0) {
                                GameOver();
                            }
                        }
                        enemy.DestroyEnemy();
                        ToiloGame.PlaySound("PlayerExplosion", 0.05f);
                        animations.Add(new AnimatedObject(ToiloGame.GetTexture("Explosion"),enemy.Position));
                    }
                    foreach (PlayerShot shot in player.Shots) {
                        if (shot.Collide(enemy)) {
                            shot.DestroyShot();
                            enemy.Life -= 20f;
                            if (enemy.Life <= 0) {
                                enemy.DestroyEnemy();
                                player.Score ++;
                                ToiloGame.PlaySound("PlayerExplosion", 0.05f);
                                animations.Add(new AnimatedObject(ToiloGame.GetTexture("Explosion"),enemy.Position));
                            }
                        }
                    }
                    foreach (Tile tile in tiles) {
                        if(ToiloGame.InBounds(tile.Position)) {
                            if(tile.Collide(enemy)) {
                                enemy.DestroyEnemy();
                                ToiloGame.PlaySound("PlayerExplosion", 0.1f);
                                animations.Add(new AnimatedObject(ToiloGame.GetTexture("Explosion"),enemy.Position));
                                if(tile is BreakableTile) {
                                    ((BreakableTile)tile).DestroyTile();
                                }
                            }
                        }
                    }
                    if (enemy is ChasingEnemy) {
                        ((ChasingEnemy)enemy).GetPosition(player.Position);
                    }
                    enemy.Update(gameTime);
                }
                else if (ToiloGame.OffMap(enemy.Position)) {
                    enemy.DestroyEnemy();
                }
            }

            foreach (Tile tile in tiles) {
                if(ToiloGame.InBounds(tile.Position)) {
                    if (!player.Invulnerable && player.Collide(tile)) {
                        animations.Add(new AnimatedObject(ToiloGame.GetTexture("Explosion"), player.Position));
                        player.DestroyPlayer();
                        if (player.Lives < 0) {
                            GameOver();
                        }
                        if (tile is BreakableTile) {
                            ((BreakableTile)tile).DestroyTile();
                        }
                    }
                    foreach (PlayerShot shot in player.Shots) {
                        if (shot.Collide(tile)) {
                            shot.DestroyShot();
                            if (tile is BreakableTile) {
                                if (!tile.Destroy) {
                                    ((BreakableTile)tile).DamageTile(20f);
                                    if (((BreakableTile)tile).Life <= 0) {
                                        animations.Add(new AnimatedObject(ToiloGame.GetTexture("Explosion"), tile.Position));
                                    }
                                }
                            }
                        }
                    }
                    tile.Update(gameTime);
                }
                else if (ToiloGame.OffMap(tile.Position)) {
                    tile.Destroy = true;
                }
            }
            foreach (AnimatedObject animation in animations) {
                if(ToiloGame.InBounds(animation.Position)) {
                    animation.Update(gameTime);
                }
                else if(ToiloGame.OffMap(animation.Position)) {
                    animation.Destroy=true;
                }
            }
            foreach (Coin coin in coins) {
                if(ToiloGame.InBounds(coin.Position)) {
                    if (player.Collide(coin)) {
                        coin.DestroyCoin();
                        ToiloGame.PlaySound("CoinPickup", 0.3f);
                        player.Score ++;
                    }
                    coin.Update(gameTime);
                }
                else if (ToiloGame.OffMap(coin.Position)) {
                    coin.DestroyCoin();
                }
            }

            if (texts.Count > 0) {
                texts[0].Update(gameTime);
            }
            hud.Update(gameTime, player.Score,player.Lives);

            if (!levelComplete && player.Lives>=0 && ToiloGame.CameraDistance.X > levelMap.Width * 32) {
                levelComplete = true;
                texts.Add(new GameText(new Vector2(ToiloGame.Width / 2, ToiloGame.Height / 2), ToiloGame.DefaultFont, "Stage Complete!", 0.2f,2f));
            }
            if (levelComplete && texts.Count<=0) {
                GoToNext();
            }
            if (gameOver && texts.Count < 1) {
                player.Reset();
                game.ChangeLevel(ToiloGame.GameMenu);
            }
        }

        public virtual void Draw(SpriteBatch spriteBatch) {
            foreach (Coin coin in coins) {
                if(ToiloGame.InBounds(coin.Position)&&!coin.Destroy) {
                    coin.Draw(spriteBatch);
                }
            }
            foreach (Enemy enemy in enemies) {
                if (ToiloGame.InBounds(enemy.Position)&&!enemy.Destroy) {
                    enemy.Draw(spriteBatch);
                }
            }
            foreach (Tile tile in tiles) {
                if(ToiloGame.InBounds(tile.Position)&&!tile.Destroy) {
                    tile.Draw(spriteBatch);
                }
            }
            foreach (AnimatedObject animation in animations) {
                if (ToiloGame.InBounds(animation.Position)&&!animation.Destroy) {
                    animation.Draw(spriteBatch);
                }
            }
            foreach (GameText text in texts) {
                text.Draw(spriteBatch);
            }
            if (player.Lives >= 0) {
                player.Draw(spriteBatch);
            }

            hud.Draw(spriteBatch);
        }

        private void LoadMap(Texture2D mapTexture) {
            if (tiles != null) {
                tiles.RemoveAll(item => item != null);
            }
            if (coins != null) {
                coins.RemoveAll(item => item != null);
            }
            if (enemies!= null) {
                enemies.RemoveAll(item => item != null);
            }
            tiles = new List<Tile>();
            coins = new List<Coin>();
            enemies = new List<Enemy>();

            Texture2D tile1 = ToiloGame.GetTexture("Tilev2");
            Texture2D tile2 = ToiloGame.GetTexture("Tilev3");
            Texture2D coin1 = ToiloGame.GetTexture("Coinv2");
            Texture2D enemyTexture = ToiloGame.GetTexture("Enemy");
            Color[] mapColor = new Color[mapTexture.Width * mapTexture.Height];
            int tileH = 32, tileW = 32;
            int xyCount = 0;

            mapTexture.GetData(mapColor);

            foreach (Color colorPoint in mapColor) {
                if (colorPoint != Color.White) {
                    Vector2 position = Vector2.UnitY * (float)(Math.Floor((float)xyCount / mapTexture.Width));
                    position.X = xyCount - (position.Y * mapTexture.Width);
                    position = new Vector2(position.X * tileW, position.Y * tileH);

                    if (colorPoint.R == 50 && colorPoint.G == 50 && colorPoint.B == 50) {
                        Tile tile = new Tile(tile1, position);
                        tiles.Add(tile);
                    }
                    else if (colorPoint.R == 255 && colorPoint.G == 255 && colorPoint.B == 0) {
                        Coin coin = new Coin(coin1, position);
                        coins.Add(coin);
                    }
                    else if (colorPoint.R == 100) {
                        MovingTile mTile = new MovingTile(tile1, position, new Vector2((int)colorPoint.G / 10, (int)colorPoint.G % 10), (int)colorPoint.B);
                        tiles.Add(mTile);
                    }
                    else if (colorPoint.R == 101) {
                        MovingTile mTile = new MovingTile(tile1, position, -new Vector2((int)colorPoint.G / 10, (int)colorPoint.G % 10), (int)colorPoint.B);
                        tiles.Add(mTile);
                    }
                    else if (colorPoint.R == 150) {
                        BreakableTile bTile = new BreakableTile(tile2, position, (float)colorPoint.G);
                        tiles.Add(bTile);
                    }
                    else if (colorPoint.R == 250) {
                        ChasingEnemy cEnemy = new ChasingEnemy(enemyTexture, position, player.Position,colorPoint.G,colorPoint.B);
                        enemies.Add(cEnemy);
                    }
                }
                xyCount++;
            }
        }

        public void SetNextLevel(Level nextL) {
            this.nextLevel = nextL;
        }

        public void SetPlayer(Player pl) {
            this.player = pl;
        }

        private void GoToNext() {
            if (nextLevel != null) {
                nextLevel.SetPlayer(player);
                game.ChangeLevel(nextLevel);
            }
            else if(!gameOver) {
                GameOver();
            }
        }
    }
}
