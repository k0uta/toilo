﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Toilo {
    public class Tile: BaseObject {
        private const int tileW = 32;
        private const int tileH = 32;
        protected bool destroy = false;

        public Tile(Texture2D texture, Vector2 position): base(texture, position, tileW, tileH) {
        }

        public override void Update(GameTime gameTime) {
            base.Update(gameTime);
        }

        public override void Draw(SpriteBatch spriteBatch) {
            spriteBatch.Draw(texture, position, Color.White);
            base.Draw(spriteBatch);
        }

        public bool Destroy {
            get { return destroy; }
            set { destroy = value; }
        }

    }
}
