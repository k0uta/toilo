﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Toilo {
    public class BreakableTile: Tile {
        protected float life;

        public BreakableTile(Texture2D texture, Vector2 position, float lif): base(texture, position) {
            this.life = lif;
        }

        public override void Update(GameTime gameTime) {
            if (life <= 0) {
                DestroyTile();
            }
            base.Update(gameTime);
        }

        public void DamageTile(float damage) {
            life -= damage;
            ToiloGame.PlaySound("PlayerExplosion", 0.02f);
        }

        public void DestroyTile() {
            destroy = true;
        }

        public float Life {
            get { return life; }
        }
    }
}
