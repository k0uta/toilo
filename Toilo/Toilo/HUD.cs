﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Toilo {
    public class HUD {
        Texture2D hudSeparator;
        Texture2D playerIcon;
        Coin coinsIcon;
        String levelName;
        int score;
        int lives;

        public HUD(Texture2D hudSep, Texture2D playerI, String levelN, int initialScore=0) {
            this.hudSeparator = hudSep;
            this.playerIcon = playerI;
            this.levelName = levelN;
            this.score = initialScore;
            coinsIcon = new Coin(ToiloGame.GetTexture("Coinv2"), Vector2.Zero);
        }

        public void Update(GameTime gameTime, int playerScore, int playerLives) {
            score = playerScore;
            lives = playerLives;
            coinsIcon.Position = new Vector2(ToiloGame.CameraDistance.X + 200, ToiloGame.Height + 21);
            coinsIcon.Update(gameTime);
        }

        public void Draw(SpriteBatch spriteBatch) {
            spriteBatch.Draw(hudSeparator, new Vector2(ToiloGame.CameraDistance.X, ToiloGame.Height), Color.White);
            spriteBatch.DrawString(ToiloGame.DefaultFont, levelName, new Vector2(ToiloGame.CameraDistance.X+5, ToiloGame.Height + 25), Color.White,0f,Vector2.Zero,0.5f,SpriteEffects.None,1f);
            coinsIcon.Draw(spriteBatch);
            spriteBatch.DrawString(ToiloGame.DefaultFont, "x" + String.Format("{0:000}",score), coinsIcon.Position + new Vector2(32,3), Color.White, 0f, Vector2.Zero, 0.5f, SpriteEffects.None, 1f);
            for (int i = 0; i < lives; i++) {
                spriteBatch.Draw(playerIcon, new Vector2(ToiloGame.CameraDistance.X + ToiloGame.Width - (40 * (i+1)), ToiloGame.Height + 15), Color.White);
            }
        }
    }
}
