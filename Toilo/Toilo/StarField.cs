﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Toilo {
    class StarField {
        List<Star> stars;
        Texture2D starTexture;
        private int maxStars;
        private int maxHeight;
        private int startX;
        Random rand = new Random();

        public StarField(Texture2D sTexture, int maxS, int maxH, int sX) {
            this.starTexture = sTexture;
            this.maxStars = maxS;
            this.maxHeight = maxH;
            this.startX = sX;
            stars = new List<Star>();
        }

        public void Update(GameTime gameTime) {
            if (stars.Count < maxStars) {
                Star newStar = new Star(starTexture, new Vector2(startX+ToiloGame.CameraDistance.X, rand.Next(0,maxHeight)),(float)rand.NextDouble());
                stars.Add(newStar);
            }
            stars.RemoveAll(item => item.Position.X < ToiloGame.CameraDistance.X);
            foreach (Star star in stars) {
                star.Update(gameTime);
            }
        }

        public void Draw(SpriteBatch spriteBatch) {
            foreach (Star star in stars) {
                star.Draw(spriteBatch);
            }
        }

        public void Restart() {
            stars.RemoveAll(item => item != null);
            stars = new List<Star>();
        }
    }
}
