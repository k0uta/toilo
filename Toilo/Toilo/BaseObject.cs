﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Toilo {
    public abstract class BaseObject {
        protected Texture2D texture;
        protected Vector2 position;
        protected Rectangle hitBox;
        protected Rectangle sourceBox;
        protected int width;
        protected int height;

        public BaseObject(Texture2D tex, Vector2 pos, int wt, int ht){
            this.texture = tex;
            this.position = pos;
            this.width = wt;
            this.height = ht;
            this.sourceBox = new Rectangle(0, 0, wt, ht);
        }

        public virtual void Draw(SpriteBatch spriteBatch) {
        }

        public virtual void Update(GameTime gameTime) {
            this.hitBox = new Rectangle((int)position.X, (int)position.Y, width, height);
        }

        public Vector2 Position {
            get { return position; }
            set { position = value; }
        }

        public bool Collide(BaseObject b2) {
            Color[] c1, c2;
            c1 = new Color[sourceBox.Width * sourceBox.Height];
            c2 = new Color[b2.sourceBox.Width * b2.sourceBox.Height];
            texture.GetData<Color>(0, sourceBox, c1, 0, c1.Length);
            b2.texture.GetData<Color>(0, b2.sourceBox, c2, 0, c2.Length);

            return IntersectPixels(hitBox, c1, b2.hitBox, c2);
        }

        static bool IntersectPixels(Rectangle rectangleA, Color[] dataA,
                            Rectangle rectangleB, Color[] dataB) {
            // Find the bounds of the rectangle intersection  
            int top = Math.Max(rectangleA.Top, rectangleB.Top);
            int bottom = Math.Min(rectangleA.Bottom, rectangleB.Bottom);
            int left = Math.Max(rectangleA.Left, rectangleB.Left);
            int right = Math.Min(rectangleA.Right, rectangleB.Right);

            // Check every point within the intersection bounds  
            for (int y = top; y < bottom; y++) {
                for (int x = left; x < right; x++) {
                    // Get the color of both pixels at this point  
                    Color colorA = dataA[(x - rectangleA.Left) +
                                         (y - rectangleA.Top) * rectangleA.Width];
                    Color colorB = dataB[(x - rectangleB.Left) +
                                         (y - rectangleB.Top) * rectangleB.Width];

                    // If both pixels are not completely transparent,  
                    if (colorA.A != 0 && colorB.A != 0) {
                        // then an intersection has been found  
                        return true;
                    }
                }
            }

            // No intersection found  
            return false;
        } 
    }
}
