using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Toilo {

    public class ToiloGame : Microsoft.Xna.Framework.Game {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        private const int safetyDistance = 100;
        static int gameWidth = 832;
        static int gameHeight = 640;

        static Vector2 cameraDistance = Vector2.Zero;
        static List<SoundEffect> sounds;
        static List<Texture2D> textures;
        static List<Song> songs;
        static SpriteFont defaultFont;
        static Menu menu;

        private Level currentLevel;
        private StarField starField;

        public ToiloGame() {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            graphics.PreferredBackBufferWidth = Width;
            graphics.PreferredBackBufferHeight = 700;
            graphics.ApplyChanges();
            Window.Title = "Shitty Space";
        }

        protected override void Initialize() {

            base.Initialize();
        }

        protected override void LoadContent() {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);
            
            sounds = new List<SoundEffect>();
            AddSound("CoinPickup");
            AddSound("PlayerExplosion");
            AddSound("PlayerShot");
            AddSound("TextWrite");

            textures = new List<Texture2D>();
            AddTexture("Playerv3");
            AddTexture("Playerv4");
            AddTexture("Shotv2");
            AddTexture("HUD");
            AddTexture("Mapv1");
            AddTexture("Mapv2");
            AddTexture("Tilev2");
            AddTexture("Tilev3");
            AddTexture("Coinv2");
            AddTexture("Enemy");
            AddTexture("Explosion");

            songs = new List<Song>();
            AddSong("MenuSong");
            PlaySong("MenuSong", 0.5f);

            defaultFont = Content.Load<SpriteFont>("GameFont");

            starField = new StarField(CreateRectangle(2, 2, Color.White), 50, Height - 2, GraphicsDevice.Viewport.Width + 100);
            Player p1 = new Player(GetTexture("Playerv4"), new Vector2(0, Height / 2), GetTexture("Shotv2"));

            menu = new Menu(this);
            Level level1 = new Level(this, GetTexture("Mapv1"),"Stage 1");
            level1.SetPlayer(p1);
            Level level2 = new Level(this, GetTexture("Mapv2"), "Stage 2");

            menu.SetNextLevel(level1);
            level1.SetNextLevel(level2);

            ChangeLevel(menu);

        }

        protected override void UnloadContent() {
        }

        protected override void Update(GameTime gameTime) {
            // Allows the game to exit
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                this.Exit();

            currentLevel.Update(gameTime);
            cameraDistance.X += (float)gameTime.ElapsedGameTime.TotalMilliseconds / 3;

            starField.Update(gameTime);
            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime) {
            GraphicsDevice.Clear(Color.Black);

            spriteBatch.Begin(SpriteSortMode.Immediate,BlendState.NonPremultiplied,null,null,null,null,Matrix.CreateTranslation(-cameraDistance.X,0,0));
            starField.Draw(spriteBatch);
            currentLevel.Draw(spriteBatch);
            spriteBatch.End();
            base.Draw(gameTime);
        }

        public Texture2D CreateRectangle(int width, int height, Color fillColor) {
            Texture2D rectangleTexture = new Texture2D(GraphicsDevice, width, height, true, SurfaceFormat.Color);// create the rectangle texture, ,but it will have no color! lets fix that
            Color[] color = new Color[width * height];//set the color to the amount of pixels
            for (int i = 0; i < color.Length; i++)//loop through all the colors setting them to whatever values we want
            {
                color[i] = fillColor;
            }
            rectangleTexture.SetData(color);//set the color data on the texture
            return rectangleTexture;
        }
        public static void PlaySound(string soundName, float volume) {
            SoundEffect sound = sounds.Find(item => item.Name == soundName);
            if (sound != null) {
                sound.Play(volume, 0, 0);
            }
        }
        public static Texture2D GetTexture(string textureName) {
            Texture2D texture = textures.Find(item => item.Name == textureName);
            return texture;
        }
        public static void PlaySong(string songName, float volume) {
            Song song = songs.Find(item => item.Name == songName);
            if (song != null) {
                MediaPlayer.Stop();
                MediaPlayer.Play(song);
                MediaPlayer.Volume = volume;
                MediaPlayer.IsRepeating = true;
            }
        }

        private void AddSound(String assetName) {
            SoundEffect newSound;
            newSound = Content.Load<SoundEffect>(assetName);
            newSound.Name = assetName;
            sounds.Add(newSound);
        }
        private void AddTexture(String assetName) {
            Texture2D newTexture;
            newTexture = Content.Load<Texture2D>(assetName);
            newTexture.Name = assetName;
            textures.Add(newTexture);
        }
        private void AddSong(String assetName) {
            Song newSong;
            newSong = Content.Load<Song>(assetName);
            songs.Add(newSong);
        }

        public void ChangeLevel(Level newLevel) {
            cameraDistance = Vector2.Zero;
            currentLevel = newLevel;
            starField.Restart();
            newLevel.Initialize();
        }

        public static bool InBounds(Vector2 position) {
            return (position.X + safetyDistance >= cameraDistance.X && position.X <= gameWidth + cameraDistance.X);
        }
        public static bool OffMap(Vector2 position) {
            return (position.X + safetyDistance < cameraDistance.X);
        }

        public static int Width {
            get { return gameWidth; }
        }
        public static int Height {
            get { return gameHeight; }
        }
        public static Menu GameMenu {
            get { return menu; }
        }
        public static Vector2 CameraDistance {
            get { return cameraDistance; }
        }
        public static SpriteFont DefaultFont {
            get { return defaultFont; }
        }
    }
}
