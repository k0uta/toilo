﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Toilo {
    class Star: BaseObject {
        float speed;
        float alpha;

        public Star(Texture2D texture, Vector2 position, float depth): base(texture, position, texture.Width, texture.Height) {
            this.speed = depth*depth*5f+1f;
            this.alpha = 255f * depth;
        }

        public override void Draw(SpriteBatch spriteBatch) {
            spriteBatch.Draw(texture, position, new Color(255,255,255,(byte) MathHelper.Clamp(alpha,0,255)));
            base.Draw(spriteBatch);
        }

        public override void Update(GameTime gameTime) {
            position.X -= speed;
            base.Update(gameTime);
        }
    }
}
