using System;

namespace Toilo {
#if WINDOWS || XBOX
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {
            using (ToiloGame game = new ToiloGame())
            {
                game.Run();
            }
        }
    }
#endif
}

