﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Toilo {
    public class ChasingEnemy: Enemy {
        protected Vector2 direction;
        protected float speed;

        private const int enemyW =32;
        private const int enemyH = 32;

        public ChasingEnemy(Texture2D texture, Vector2 position, Vector2 targetPosition, float life = 50f, float speed = 100f): base(texture, position, enemyW, enemyH, life) {
            this.speed = speed;
            this.direction = targetPosition - position;
            this.direction.Normalize();
        }

        public override void Update(GameTime gameTime) {
            position += direction*speed;
            base.Update(gameTime);
        }

        public void GetPosition(Vector2 targetPosition) {
            direction = targetPosition - position;
            direction.Normalize();
        }

    }
}
