﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Toilo {
    public class PlayerShot: BaseObject {
        private const int shotW = 8;
        private const int shotH = 8;
        private const float speed = 1000f;

        private bool destroy = false;

        public PlayerShot(Texture2D tex, Vector2 pos): base(tex, pos, shotW, shotH) {

        }

        public override void Draw(SpriteBatch spriteBatch) {
            spriteBatch.Draw(texture, position, sourceBox, Color.White);
            base.Draw(spriteBatch);
        }

        public override void Update(GameTime gameTime) {
            position.X += ((float)gameTime.ElapsedGameTime.TotalMilliseconds / 1000) * speed;
            base.Update(gameTime);
        }

        public void DestroyShot() {
            destroy = true;
        }

        public bool Destroy {
            get { return destroy; }
        }
    }
}
