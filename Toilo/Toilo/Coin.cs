﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Toilo {
    public class Coin: BaseObject {
        private const int frameW = 32;
        private const int frameH = 32;
        private const float fpsTime = 0.1f;

        private float fpsTimer = 0f;

        private bool destroy = false;

        public Coin(Texture2D tex, Vector2 pos): base(tex, pos, frameW, frameH) {
            sourceBox = new Rectangle(0, 0, frameH, frameW);
        }

        public override void Update(GameTime gameTime) {
            if (fpsTimer > fpsTime) {
                fpsTimer = 0f;
                if (sourceBox.X/frameW >= texture.Width/frameW -1) {
                    sourceBox = new Rectangle(0, 0, frameW, frameH);
                }
                else {
                    sourceBox = new Rectangle(sourceBox.X + frameW, sourceBox.Y, frameW, frameH);
                }
            }
            else {
                fpsTimer += (float)gameTime.ElapsedGameTime.Milliseconds / 1000;
            }
            base.Update(gameTime);
        }

        public override void Draw(SpriteBatch spriteBatch) {
            spriteBatch.Draw(texture, position, sourceBox, Color.White);
            base.Draw(spriteBatch);
        }

        public void DestroyCoin() {
            destroy = true;
        }

        public bool Destroy {
            get { return destroy; }
        }
    }
}
